# GameRadio

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Fake API
go to ./face_api/
```
node ./index.js
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).


### Used libs
[AXIOS](https://www.npmjs.com/package/axios) | [MIT License](https://github.com/imcvampire/vue-axios/blob/master/LICENSE)

[VueAxios](https://www.npmjs.com/package/vue-axios) | [MIT License](https://github.com/imcvampire/vue-axios/blob/master/LICENSE)

[Bootstrap](https://getbootstrap.com/) | [MIT](https://github.com/twbs/bootstrap/blob/v4-dev/LICENSE)

[howler.js](https://github.com/goldfire/howler.js) - for audio | [MIT](https://github.com/goldfire/howler.js/blob/master/LICENSE.md)

[Vue Slide Bar](https://www.npmjs.com/package/vue-slide-bar) | [MIT](https://github.com/biigpongsatorn/vue-slide-bar/blob/HEAD/LICENSE)

[vue-perfect-scrollbar](https://www.npmjs.com/package/vue-perfect-scrollbar) | [MIT](https://github.com/Degfy/vue-perfect-scrollbar#license)

[ClickOutside](https://www.npmjs.com/package/vue-click-outside) | [MIT](https://github.com/vue-bulma/click-outside/blob/master/LICENSE)
