import Player from '../../services/player-init';

let player = null;
let timerId = null;

const state = {
  playlist: [],
  currentTrack: {},
  showPlayer: false,
  player: null,
  isPlaying: false,
  volumePlayer: 50,
  longLine: 0,
  progressWidth: 0,
  trackDuration: 0
};

const getters = {
  getPlayList: state => state.playlist,
  getCurrentTrack: state => state.currentTrack,
  getVisiblePlayer: state => state.showPlayer,
  getStatusPlaying: state => state.isPlaying,
  getProgressWidth: state => state.progressWidth,
  getTrackDuration: state => state.trackDuration
};

const mutations = {
  setPlaylist: (state, payload) => {
    state.playlist = payload;
  },
  setCurrentTrack: (state, payload) => {
    state.currentTrack = payload;
  },
  setVisiblePlayer: (state, payload) => {
    state.showPlayer = payload;
  },
  setPlayer: (state, pl) => {
    if (player) {
      if (pl.playlist[0].id !== player.playlist[0].id) {
        player.destroyPlayer();
        player = new Player(pl.playlist);
        state.player = player;
      }
    } else {
      player = new Player(pl.playlist);
      state.player = player;
    }
  },
  startPlay(state, idItm) {
    if (state.currentTrack.indx === idItm.indx) {
      player.play(idItm.indx);
    } else {
      player.skipTo(idItm.indx);
    }
    state.isPlaying = true;
  },
  pausePlayer() {
    player.pause();
    state.isPlaying = false;
  },
  continuePlay() {
    console.log('player', player);
    player.play();
    state.isPlaying = true;
  },
  setVolumePlayer(state, pl) {
    state.volumePlayer = pl;
    player.setVolume(pl / 100);
  },
  skipTrak(state, dir) {
    let trackIndx = player.skipPrevNext(dir);
    state.currentTrack = state.playlist[trackIndx];
  },
  animateProgress(state, pl) {
    if (pl) {
      timerId = setInterval(function() {
        try {
          let playerData = player.step();
          if (playerData) {
            state.progressWidth = playerData.progress;
            state.trackDuration = playerData.duration;
            if (playerData.progress < 1) {
              state.currentTrack = state.playlist[player.index];
            }
          }
        } catch (e) {
          clearInterval(timerId);
        }
      }, 20);
    } else {
      setTimeout(function() {
        clearInterval(timerId);
        timerId = null;
      }, 0);
    }
  },
  destroyPlayer() {
    if (player) {
      console.log('destroyPlayer');
      player.destroyPlayer();
      player = null;
    }
  }
};

const actions = {
  START_PLAY: async ({ commit, dispatch }, payload) => {
    console.log(payload);
    await dispatch('radio/STOP_RADIO', null, { root: true });
    commit('setPlayer', payload);
    commit('setPlaylist', payload.playlist);
    commit('startPlay', payload.track);
    commit('setCurrentTrack', payload.track);
    commit('setVisiblePlayer', true);
    commit('animateProgress', true); // start progress bar
  },
  PAUSE_PLAYER: ({ commit }) => {
    commit('pausePlayer');
    commit('animateProgress', false); // stop progress bar
  },
  CONTINUE_PLAY: ({ commit }) => {
    commit('continuePlay');
  },
  SET_VOLUME_PLAYER: ({ commit }, val) => {
    commit('setVolumePlayer', val);
  },
  SKIP_TRAK: ({ commit }, dir) => {
    commit('skipTrak', dir);
  },
  SEEK_POSITION_TRACK: (context, per) => {
    player.seek(per);
  },
  DESTORY_PLAYER: ({ commit }) => {
    commit('destroyPlayer');
    commit('setPlaylist', []);
    commit('setCurrentTrack', {});
    commit('setVisiblePlayer', false);
    commit('animateProgress', false); // kill watching progress bar
  }
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
};
