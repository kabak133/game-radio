import { PodcastService } from '../../common/api.service';

const state = {
  podcastCategory: [],
  podcastListSeparCategory: {},
  reports: [],
  reportsType: [],
  streamsList: [],
  podcastsList: [],
  chooseType: 1,
  chooseList: [],
  playListForPlayer: []
};

const getters = {
  getPodcastCategory: state => state.podcastCategory,
  //getReports: state => state.reports,
  getReportsType: state => state.reportsType,
  getRreports: state => state.getRreports,
  getChoosenType: state => state.chooseType,
  podcastListSeparCategory: state => {
    let separeteReports = {};
    for (let type of state.reportsType) {
      separeteReports[type.name] = [];
      for (let rep of state.reports) {
        if (rep.type === type.id) {
          separeteReports[type.name].push(rep);
        }
      }
    }
    return separeteReports;
  }
};

const mutations = {
  setCagegory: (state, payload) => {
    state.podcastCategory = payload;
  },
  setReports: (state, payload) => {
    state.reports = payload;
  },
  setReportsType: (state, payload) => {
    state.reportsType = payload;
  },
  setChooseType: (state, idType) => {
    state.chooseType = idType;
  },
  setPlaylist(state, pl) {
    console.log('setPlaylist', pl);
    let playList = [];

    for (let rep of state.reports) {
      if (rep.type === pl.type) {
        playList.push(rep);
      }
    }
    state.playListForPlayer = playList;
    this.dispatch(
      'player/START_PLAY',
      { playlist: playList, track: pl },
      { root: true }
    );
  }
};

const actions = {
  GET_PODCAST_CATEGORY: async ({ commit }) => {
    let { data } = await PodcastService.getPodcastCategories();
    commit('setCagegory', data.categories);
  },
  GET_REPORTS: async ({ commit }) => {
    let { data } = await PodcastService.getReportsData();
    console.log(data);
    commit('setReportsType', data.reportsType);
    commit('setReports', data.reports);
  },
  CHOOSE_TYPE: ({ commit }, idType) => {
    commit('setChooseType', idType);
  },
  START_PLAY: ({ commit }, payload) => {
    commit('setPlaylist', payload);
  }
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
};
