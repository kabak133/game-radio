import { PlayListService } from '../../common/api.service';

const state = {
  playlist: [],
  chosenPlaylist: [],
  chosenPlaylistSlug: '',
  playListForPlayer: [],
  filterArr: []
};

const getters = {
  playlistKeys: state => Object.keys(state.playlist),
  chosenList: state => state.chosenPlaylist,
  chosenListSlug: state => state.chosenPlaylistSlug,
  getFilterPL: state => state.filterArr
};

const mutations = {
  setPalylist: (state, payload) => {
    state.playlist = payload;
    /*TODO: delete after make back-end*/
    state.chosenPlaylist = state.playlist[Object.keys(state.playlist)[0]];
    state.chosenPlaylistSlug = Object.keys(state.playlist)[0];
    state.filterArr = filterPlaylist(state.chosenPlaylist);
  },
  setChooseList: (state, slug) => {
    state.chosenPlaylist = state.playlist[slug];
  },
  setSlugChooseList: (state, slug) => {
    state.chosenPlaylistSlug = slug;
  },
  setPlayListForPlayer: (state, slugList) => {
    let choosePlaylist = state.playlist[slugList];
    state.filterArr = filterPlaylist(choosePlaylist);
  }
};

const actions = {
  GET_PLAYLIST: async ({ commit }) => {
    await PlayListService.getPlayList().then(r => {
      console.log(r);
      commit('setPalylist', r.data);
    });
  },
  CHOOSE_PLAYLIST: ({ commit }, slugList) => {
    commit('setChooseList', slugList);
    commit('setSlugChooseList', slugList);
    commit('setPlayListForPlayer', slugList);
  },
  PLAY_TRACK: ({ state, dispatch }, playload) => {
    let dataForPlayer = {
      playlist: state.filterArr,
      track: playload
    };
    dispatch('player/START_PLAY', dataForPlayer, { root: true });
  }
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
};

function filterPlaylist(arr) {
  let filterArr = [];
  for (let trak of arr) {
    if (trak.src) {
      filterArr.push(trak);
    }
  }
  return filterArr;
}
