import Radio from '../../services/radio-init';

let radio = null;

const state = {
  radio: null,
  radioState: false,
  rangeValue: 50,
  visibilityGlobalControl: false
};

const getters = {
  getVisibilityGlobalControl: state => state.visibilityGlobalControl,
  getRadioState: state => state.radioState,
  getRadioVolume: state => state.rangeValue
};

const mutations = {
  setRadio: state => {
    if (!state.radio) {
      radio = new Radio([
        {
          freq: '90.4',
          title: 'Radio Rock',
          src: 'http://online-radioroks.tavrmedia.ua/RadioROKS',
          howl: null
        }
      ]);
      state.radio = radio;
    }
  },
  startRadio: state => {
    /*play*/
    if (radio) {
      radio.play();
    }
    state.radioState = true;
    state.visibilityGlobalControl = true;
  },
  stopRadio: state => {
    /*stop*/
    if (radio) {
      radio.stop();
      radio = null;
    }
    state.radio = null;
    state.radioState = false;
    state.visibilityGlobalControl = false;
  },
  pauseRadio: state => {
    /*pause*/
    radio.pause();
    state.radioState = false;
  },
  setVolume: (state, pl) => {
    state.rangeValue = pl;
    radio.setVolume(pl / 100);
  }
};

const actions = {
  TOGGLE_RADIO: ({ commit, dispatch }, pl) => {
    if (!radio) {
      commit('setRadio');
      dispatch('player/DESTORY_PLAYER', null, { root: true });
    }
    if (pl) {
      commit('startRadio');
    } else {
      commit('pauseRadio');
    }
  },
  SET_VOLUME_RADIO: ({ commit }, pl) => {
    commit('setVolume', pl);
  },
  STOP_RADIO: ({ commit }) => {
    commit('stopRadio');
  }
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
};
