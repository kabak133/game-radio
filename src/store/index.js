import Vue from 'vue';
import Vuex from 'vuex';

import radio from './modules/radio';
import playlist from './modules/play-list';
import podcast from './modules/podcast';
import player from './modules/player';

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    radio,
    playlist,
    podcast,
    player
  }
});
