export default function dNumbers(d) {
  const str = String(d);
  return str.replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ');
}
