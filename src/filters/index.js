import time from './time';
import dNumbers from './displayNumbers';

export default {
  install(Vue) {
    Vue.filter('time', time);
    Vue.filter('dNumbers', dNumbers);
  }
};
