import { Howl, Howler } from 'howler';

class Player {
  constructor(playlist, index) {
    this.playlist = playlist;
    this.index = index || 0;
    console.log('playlist', playlist)







  }

  /**
   * Play a song in the playlist.
   * @param  {Number} index Index of the song in the playlist (leave empty to
   *   play the first or current).
   */

  play(index) {
    let self = this;
    let sound;

    index = typeof index === 'number' ? index : self.index;
    let data = self.playlist[index];

    console.log(data);
    // If we already loaded this track, use the current one.
    // Otherwise, setup and load a new Howl.
    if (data.howl) {
      sound = data.howl;
    } else {
      sound = data.howl = new Howl({
        src: [data.src],
        html5: true, // Force to HTML5 so that the audio can stream in (best
        // for large files).
        onend: function() {
          self.skipPrevNext('next');
        },
        onplay: function() {



          self.mode = 'freq'; // or 'wave'
          self.theme = 'classic';
          self.lcd = document.querySelector('.lcd');
          self.timerLcd = null;
          self.spectrum = document.querySelector('.spectrum');

          self.analyser = Howler.ctx.createAnalyser();
          Howler.masterGain.connect(self.analyser);
          self.analyser.connect(Howler.ctx.destination);
          self.analyser.fftSize = 64;
          self.analyser.smoothingTimeConstant = 0.25;
          self.analyser.minDecibels = -128;
          self.analyser.maxDecibels = -16;
          self.bufferLength = self.analyser.frequencyBinCount;
          self.createSpectrum();


        }
      });


    }

    // Begin playing the sound.
    sound.play();

    // Keep track of the index we are currently playing.
    self.index = index;

    setInterval(() => {
      this.update();
    }, 150);
  }

  destroyPlayer() {
    console.log('destroyPlayer');

    let self = this;

    // Get the Howl we want to manipulate.
    let sound = self.playlist[self.index].howl;
    // Stop the sound.
    if (sound) {
      sound.unload();
    }
  }

  /**
   * Skip to the next or previous track.
   * @param  {String} direction 'next' or 'prev'.
   */
  skipPrevNext(direction) {
    console.log('direction', direction);
    var self = this;
    // Get the next track based on the direction of the track.
    var index = 0;

    if (direction === 'prev' || direction === false) {
      index = self.index - 1;
      console.log('prev');
      if (index < 0) {
        index = self.playlist.length - 1;
      }
    } else {
      console.log('next');
      index = self.index + 1;
      if (index >= self.playlist.length) {
        index = 0;
      }
    }

    self.skipTo(index);
    return index; // for vuex player control
  }

  /**
   * Skip to a specific track based on its playlist index.
   * @param  {Number} index Index in the playlist.
   */
  skipTo(index) {
    let self = this;
    console.log('skipTo', index);
    // Stop the current track.
    if (self.playlist[self.index].howl) {
      self.playlist[self.index].howl.stop();
      self.playlist[self.index].howl.unload();
      self.playlist[self.index].howl = null;
    }

    // Reset progress.
    // progress.style.width = '0%';

    // Play the new track.
    self.play(index);
  }

  pause() {
    var self = this;
    console.log('pause');
    // Get the Howl we want to manipulate.
    var sound = self.playlist[self.index].howl;

    // Puase the sound.
    sound.pause();

    // Show the play button.
    //playBtn.style.display = 'block';
    //pauseBtn.style.display = 'none';
  }

  currentTrack() {
    let self = this;
    return self.playlist[self.index];
  }

  /**
   * Set the volume and update the volume slider display.
   * @param  {Number} val Volume between 0 and 1.
   */
  setVolume(val) {
    // Update the global volume (affecting all Howls).
    Howler.volume(val);
  }

  /**
   * The step called within requestAnimationFrame to update the playback
   * position.
   */
  step() {
    var self = this;

    // Get the Howl we want to manipulate.
    var sound = self.playlist[self.index].howl;

    // Determine our current seek position.
    var seek = sound.seek() || 0;

    // timer.innerHTML = self.formatTime(Math.round(seek));
    let styleWidth = (seek / sound.duration()) * 100 || 0;

    // If the sound is still playing, continue stepping.
    if (sound.playing()) {
      //requestAnimationFrame(self.step.bind(self));
      return {
        progress: styleWidth,
        duration: sound.duration()
      };
    }
  }

  /**
   * Seek to a new position in the currently playing track.
   * @param  {Number} per Percentage through the song to skip.
   */
  seek(per) {
    var self = this;

    // Get the Howl we want to manipulate.
    var sound = self.playlist[self.index].howl;

    // Convert the percent into a seek position.
    if (sound.playing()) {
      sound.seek(sound.duration() * per);
    }
  }

  update() {
    console.log('this.bufferLength', frequencyBinCount)
    this.dataArray = new Uint8Array(this.bufferLength);

    if (this.mode == 'freq')
      this.analyser.getByteFrequencyData(this.dataArray);
    else if (this.mode == 'wave')
      this.analyser.getByteTimeDomainData(this.dataArray);

    this.dB.forEach((e,i) => {
      e.style.setProperty('--y', `${ this.dataArray[i] }px`);
    });
  }






  createSpectrum() {
    this.spectrum.innerHTML = '';  // fix Parcel HMR
    for (let i = 0; i < this.analyser.frequencyBinCount; i++) {
      const div = document.createElement('div');
      this.spectrum.appendChild(div);
    }
    this.dB = Array.from(document.querySelectorAll('.spectrum div'));
  }


}

/*function animateVolume() {
  // Create an analyser node in the Howler WebAudio context
  var analyser = Howler.ctx.createAnalyser();

  // Connect the masterGain -> analyser (disconnecting masterGain -> destination)
  Howler.masterGain.connect(analyser);

  // Connect the analyser -> destination
  analyser.connect(Howler.ctx.destination);


  var context = new AudioContext();
  /!*var context = new AudioContext();
  var src = context.createMediaElementSource(audio);
  var analyser = context.createAnalyser();*!/

  var canvas = document.getElementById("canvas");
  canvas.width = window.innerWidth;
  canvas.height = window.innerHeight;
  var ctx = canvas.getContext("2d");

  //src.connect(analyser);
  analyser.connect(Howler.ctx.destination);

  analyser.fftSize = 256;

  var bufferLength = analyser.frequencyBinCount;
  console.log(bufferLength);

  var dataArray = new Uint8Array(bufferLength);

  var WIDTH = canvas.width;
  var HEIGHT = canvas.height;

  var barWidth = (WIDTH / bufferLength) * 2.5;
  var barHeight;
  var x = 0;

  function renderFrame() {
    requestAnimationFrame(renderFrame);

    x = 0;

    analyser.getByteFrequencyData(dataArray);

    ctx.fillStyle = "#000";
    ctx.fillRect(0, 0, WIDTH, HEIGHT);
    for (var i = 0; i < bufferLength; i++) {
      barHeight = dataArray[i];

      var r = barHeight + (25 * (i/bufferLength));
      var g = 250 * (i/bufferLength);
      var b = 50;

      ctx.fillStyle = "rgb(" + r + "," + g + "," + b + ")";
      ctx.fillRect(x, HEIGHT - barHeight, barWidth, barHeight);

      x += barWidth + 1;
    }
  }
  renderFrame()
}*/

export default Player;
