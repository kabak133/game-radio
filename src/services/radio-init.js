import { Howl } from 'howler';

/**
 * Radio class containing the state of our stations.
 * Includes all methods for playing, stopping, etc.
 * @param {Array} stations Array of objects with station details ({title, src,
 *   howl, ...}).
 */
class Radio {
  constructor(stations) {
    this.stations = stations;
    this.index = 0;
    this.volume = 0.5;
  }

  /**
   * Play a station with a specific index.
   * @param  {Number} index Index in the array of stations.
   */
  play(index) {
    var self = this;
    var sound;

    index = typeof index === 'number' ? index : self.index;
    var data = self.stations[index];

    // If we already loaded this track, use the current one.
    // Otherwise, setup and load a new Howl.
    if (data.howl) {
      sound = data.howl;
    } else {
      sound = data.howl = new Howl({
        src: data.src,
        html5: true, // A live stream can only be played through HTML5 Audio.
        format: ['mp3', 'aac'],
        volume: self.volume
      });
    }

    // Begin playing the sound.
    sound.play();

    // Keep track of the index we are currently playing.
    self.index = index;
  }

  /**
   * Stop a station's live stream.
   */
  stop() {
    var self = this;

    // Get the Howl we want to manipulate.
    var sound = self.stations[self.index].howl;

    // Stop the sound.
    if (sound) {
      sound.unload();
    }
  }

  pause() {
    var self = this;

    // Get the Howl we want to manipulate.
    var sound = self.stations[self.index].howl;

    // Stop the sound.
    if (sound) {
      sound.pause();
    }
  }
  setVolume(val) {
    var self = this;
    self.volume = val;
    // Get the Howl we want to manipulate.
    var sound = self.stations[self.index].howl;

    if (sound) {
      sound.volume(val);
    }
  }
}

export default Radio;

/*// Setup our new radio and pass in the stations.
var radio = new Radio([
  {
    freq: '90.4',
    title: 'Radio Rock',
    src: 'http://online-radioroks.tavrmedia.ua/RadioROKS',
    howl: null
  }
]);*/
