import Vue from 'vue';
import Router from 'vue-router';

Vue.use(Router);

const routes = [
  {
    path: '/',
    name: 'radio',
    component: () => import('../views/RadioPage.vue')
  },
  {
    path: '/about',
    name: 'about',
    component: () => import('../views/AboutPage.vue')
  },
  {
    path: '/portfolio',
    name: 'portfolio',
    component: () => import('../views/PortfolioPage.vue')
  },
  {
    path: '/events',
    name: 'events',
    component: () => import('../views/EventsPage.vue')
  },
  {
    path: '/contact-us',
    name: 'contact',
    component: () => import('../views/ContactUsPage.vue')
  },
  {
    path: '/ui',
    name: 'ui',
    component: () => import('../views/UI.vue')
  },
  {
    path: '/playlists',
    name: 'playlists',
    component: () => import('../views/PlaylistsPage.vue')
  },
  {
    path: '/podcast',
    name: 'podcast',
    component: () => import('../views/PodcastPage.vue')
  },
  {
    path: '/*',
    name: 'NotFoundPage',
    component: () => import('../views/NotFoundPage.vue')
  }
];

export default new Router({
  mode: 'history',
  linkActiveClass: 'active',
  linkExactActiveClass: 'exact-active',
  routes
});
